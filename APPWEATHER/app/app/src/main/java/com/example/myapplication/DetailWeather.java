package com.example.myapplication;

public class DetailWeather {
    private String icon;
    private String temp;
    private String hum;
    private String status;
    private String wind;
    private String cloud;
    private String date;

    public DetailWeather(String icon, String temp, String hum, String status, String wind, String cloud, String date) {
        this.icon = icon;
        this.temp = temp;
        this.hum = hum;
        this.status = status;
        this.wind = wind;
        this.cloud = cloud;
        this.date = date;
    }

    public String getIcon() {
        return icon;
    }

    public String getTemp() {
        return temp;
    }

    public String getHum() {
        return hum;
    }

    public String getStatus() {
        return status;
    }

    public String getWind() {
        return wind;
    }

    public String getCloud() {
        return cloud;
    }

    public String getDate() {
        return date;
    }
}
