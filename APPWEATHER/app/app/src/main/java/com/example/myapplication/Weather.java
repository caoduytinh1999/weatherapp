package com.example.myapplication;

public class Weather {
    String day;
    String status;
    String image;
    String minTemp;
    String maxTemp;

    public Weather(String day, String status, String image, String minTemp, String maxTemp) {
        this.day = day;
        this.status = status;
        this.image = image;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    public String getDay() {
        return day;
    }

    public String getStatus() {
        return status;
    }

    public String getImage() {
        return image;
    }

    public String getMinTemp() {
        return minTemp;
    }

    public String getMaxTemp() {
        return maxTemp;
    }
}
