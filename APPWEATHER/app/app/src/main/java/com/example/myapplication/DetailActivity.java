package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    TextView txtCity,txtNation,txtTemp,txtHum,txtWind,txtStatus,txtDate,txtClouds;
    Button btnBack;
    ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Mapped();
        Assign();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void Assign(){
        Intent intent = getIntent();
        txtCity.setText("Ten Thanh Pho : " + intent.getStringExtra("city"));
        txtNation.setText("Ten Quoc Gia : " + intent.getStringExtra("nation"));
        Picasso.with(this).load("http://openweathermap.org/img/wn/"+ intent.getStringExtra("icon") +".png").into(icon);
        txtTemp.setText(intent.getStringExtra("temp")+ " °C");
        txtHum.setText(intent.getStringExtra("hum") +"%");
        txtWind.setText(intent.getStringExtra("wind")+ "m/s");
        txtDate.setText(intent.getStringExtra("date"));
        txtClouds.setText(intent.getStringExtra("cloud") +"%");
        txtStatus.setText(intent.getStringExtra("status"));

    }

    public void Mapped(){
        txtCity = findViewById(R.id.txtNameCity3);
        txtNation = findViewById(R.id.txtNation3);
        txtTemp = findViewById(R.id.txtTempe3);
        txtStatus = findViewById(R.id.txtStatus3);
        txtHum = findViewById(R.id.txtHum3);
        txtClouds = findViewById(R.id.txtCloud3);
        txtWind = findViewById(R.id.txtWind3);
        txtDate = findViewById(R.id.txtDate3);
        icon = findViewById(R.id.imgIcon3);
        btnBack = findViewById(R.id.btnBack);
    }
}