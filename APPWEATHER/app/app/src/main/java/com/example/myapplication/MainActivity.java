package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    EditText editCity;
    Button btnOK,btnNext;
    TextView txtCity,txtNation,txtTempe,txtStatus,txtWind,txtHum,txtCloud,txtDate;
    ImageView imgIcon;
    String city = "SAIGON";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Mapped();
        getCurrenWeatherData(city);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city = editCity.getText().toString();
                getCurrenWeatherData(city);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,DetailWeather.class);
                intent.putExtra("nameCity",city);
            }
        });
    }

    public void getCurrenWeatherData(String nameCity){
        String url = "http://api.openweathermap.org/data/2.5/weather?q="+ nameCity+"&units=metric&appid=25846c2317d261e79051c7048ffe1d43";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);

                    String date = object.getString("dt");
                    long l = Long.valueOf(date);
                    Date date1 = new Date(l*1000L);
                    SimpleDateFormat format = new SimpleDateFormat("EEEE yyyy-MM-dd");
                    String currenDate = format.format(date1);
                    //Toast.makeText(MainActivity.this, currenDate, Toast.LENGTH_SHORT).show();
                    txtDate.setText(currenDate);

                    String nameCity = object.getString("name");
                    txtCity.setText("Ten Thanh Pho : " + nameCity);

                    JSONArray array = object.getJSONArray("weather");
                    JSONObject weather = array.getJSONObject(0);

                    String status = weather.getString("main");
                    txtStatus.setText(status);

                    String icon = weather.getString("icon");
                    Picasso.with(MainActivity.this).load("http://openweathermap.org/img/wn/"+ icon +".png").into(imgIcon);

                    JSONObject main = object.getJSONObject("main");

                    String temp = main.getString("temp");
                    double d = Double.valueOf(temp);
                    int tempe = (int) d;
                    txtTempe.setText(String.valueOf(tempe+" C"));

                    String hum = main.getString("humidity");
                    txtHum.setText(hum + "%");

                    JSONObject wind = object.getJSONObject("wind");
                    String speed = wind.getString("speed");
                    txtWind.setText(speed+"m/s");

                    JSONObject cloud = object.getJSONObject("clouds");
                    String clouds = cloud.getString("all");
                    txtCloud.setText(clouds+"%");

                    JSONObject sys = object.getJSONObject("sys");
                    String country = sys.getString("country");
                    txtNation.setText("Ten Quoc Gia : " + country);




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);
    }

    public void Mapped(){
        editCity = findViewById(R.id.editCity);
        btnOK = findViewById(R.id.btnOK);
        btnNext = findViewById(R.id.btnBack);
        txtCity = findViewById(R.id.txtNameCity3);
        txtNation = findViewById(R.id.txtNation3);
        txtCloud = findViewById(R.id.txtCloud3);
        txtWind = findViewById(R.id.txtWind3);
        txtHum = findViewById(R.id.txtHum3);
        txtTempe = findViewById(R.id.txtTempe3);
        txtStatus =findViewById(R.id.txtStatus3);
        txtDate = findViewById(R.id.txtDate3);
        imgIcon = findViewById(R.id.imgIcon3);
    }
}