package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {

    ImageView imgBack;
    TextView txtCity;
    ListView listView;
    List<Weather> list;
    List<DetailWeather> listDetail;
    WeatherAdapter adapter;
    String city = "",nation = "",imgicon = "",temp = "",hum = "",statu = "",wind = "",clouds = "",date = "";
    int pos = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Mapped();
        Intent intent = getIntent();
        String name = intent.getStringExtra("nameCity");
        get7DayData(name);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent1 = new Intent(MainActivity2.this,DetailActivity.class);
                DetailWeather weather = listDetail.get(position);

                intent1.putExtra("city",city);
                intent1.putExtra("nation",nation);
                intent1.putExtra("icon",weather.getIcon());
                intent1.putExtra("status",weather.getStatus());
                intent1.putExtra("temp",weather.getTemp());
                intent1.putExtra("hum",weather.getHum());
                intent1.putExtra("wind",weather.getWind());
                intent1.putExtra("cloud",weather.getCloud());
                intent1.putExtra("date",weather.getDate());
                startActivity(intent1);
            }
        });
    }

    public void Mapped(){
        imgBack = findViewById(R.id.imgBack);
        txtCity = findViewById(R.id.txtnameCity2);
        listView = findViewById(R.id.listView);
        list = new ArrayList<>();
        listDetail = new ArrayList<>();
        adapter = new WeatherAdapter(this,list);
        listView.setAdapter(adapter);
    }

    public void get7DayData(String data){
        String url = "http://api.openweathermap.org/data/2.5/forecast/daily?q="+ data +"&units=metric&cnt=15&appid=53fbf527d52d4d773e828243b90c1f8e";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject objCity = object.getJSONObject("city");
                    String nameCity = objCity.getString("name");
                    txtCity.setText("Ten Thanh Pho : " + nameCity);
                    city = nameCity;
                    nation = objCity.getString("country");

                    JSONArray array = object.getJSONArray("list");
                    for (int  i = 0 ; i < array.length() ; i++){
                        JSONObject obj = array.getJSONObject(i);
                        String day = obj.getString("dt");
                        long l = Long.valueOf(day);
                        Date date1 = new Date(l*1000L);
                        SimpleDateFormat format = new SimpleDateFormat("EEEE yyyy-MM-dd");
                        String currenDate = format.format(date1);

                        JSONObject objTem = obj.getJSONObject("temp");
                        double d = Double.valueOf(objTem.getString("max"));
                        int maxtem = (int) d;
                        String maxTem = String.valueOf(maxtem);
                        double d1 = Double.valueOf(objTem.getString("min"));
                        int mintem = (int) d1;
                        String minTem = String.valueOf(mintem);

                        JSONArray arrWea = obj.getJSONArray("weather");
                        JSONObject objwea = arrWea.getJSONObject(0);
                        String status = objwea.getString("description");
                        String icon = objwea.getString("icon");

                        list.add(new Weather(currenDate,status,icon,minTem,maxTem));

                        ////////////////////////////////////////////////////////

                        imgicon = icon;
                        temp = String.valueOf((maxtem + mintem) / 2);
                        hum = obj.getString("humidity");
                        statu = status;
                        wind = obj.getString("speed");
                        clouds = obj.getString("clouds");
                        date = currenDate;

                        listDetail.add(new DetailWeather(imgicon,temp,hum,statu,wind,clouds,date));
                    }

                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);
    }

}