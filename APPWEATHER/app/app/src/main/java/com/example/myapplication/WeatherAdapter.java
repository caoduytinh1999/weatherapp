package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class WeatherAdapter  extends BaseAdapter {
    Context context;
    List<Weather> list;

    public WeatherAdapter(Context context, List<Weather> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class ViewHolder {
        TextView txtDay,txtStatus,txtminTemp,txtmaxTemp;
        ImageView imgIcon;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_listview,null);

            holder.txtDay = convertView.findViewById(R.id.txtDate2);
            holder.txtStatus = convertView.findViewById(R.id.txtStatus2);
            holder.txtminTemp = convertView.findViewById(R.id.minTemp);
            holder.txtmaxTemp = convertView.findViewById(R.id.maxTemp);
            holder.imgIcon = convertView.findViewById(R.id.imgIcon2);

            convertView.setTag(holder);

        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        Weather weather = list.get(position);
        holder.txtDay.setText(weather.getDay());
        holder.txtStatus.setText(weather.getStatus());
        holder.txtminTemp.setText(weather.getMinTemp() + "°C");
        holder.txtmaxTemp.setText(weather.getMaxTemp() + "°C");
        Picasso.with(context).load("http://openweathermap.org/img/wn/"+ weather.getImage() +".png").into(holder.imgIcon);

        return convertView;
    }
}
